# coffeine

**Keeps your computer awake at day (& night)**

... as long as your desktop supports the org.freedesktop.ScreenSaver standard.

coffeine is Free software and is licensed under the GNU GPL v3 or any
later version (at your option), however it can make you addicted. You
have been warned.

It is available in mobian repositories by default and you can simply
 sudo apt install coffeine
on these. In version 0.2 the binary is around 30kb and contains no
dependencies that a stock mobian installation would not have by default.

How to use? Use the terminal, Luke, and type 'coffeine YOURPROGRAMHERE'

## FAQ
### Why "coffeine"?

Glad you asked. I intended to call it caffeine, but that exists already. It
is a Windows-app that simulates key presses to achieve a similar purpose. Uggh,
what an ugly hack :-). Being a German and addicted to "Coffein", this then
became "coffeine". I should probably have named it *Coffein*, but .... spilled milk
and all that. At least it is easy to find in search engines once you convince 
them it is not a typo.

### How can I automatically invoke it when doing XXX?

Example for the movie play mpv follows:

  $ sudo apt install coffeine
  $ mkdir -p ~/.local/share/applications || echo "Already there"
  $ cp /usr/share/applications/mpv.desktop ~/.local/share/applications
  Edit ~/.local/share/applications/mpv.desktop and replace "mpv" with "coffeine mpv" in the Exec and TryExec lines.

or just type "coffeine your --command --here" whenever you need it.

### Does my Desktop support that org.freedesktop.ScreenSaver standard?

You can check by issueing:
  gdbus introspect -e -d org.freedesktop.ScreenSaver -o /org/freedesktop/ScreenSaver

(assuming you have the gdbus binary installed)

### Are there any drawbacks?

If you execute it in a Desktop environment that does NOT support inhibiting,
the execution will fail, I prefer that over a silent failure.

### Should that not be a functionality that is on the core operating system?

It is (credit where it is due), therefore this app is so small.
It is just the task of a program to inhibit the screensaver, and many apps do not.
So coffeine comes in handy for those programs and politely asks the screensaver
to go out of the way.

### Doesn't this exist already?

*After* I created coffeine, I discovered that if you are on gnome,
there might be _gnome-session-inhibit_ (part of the gnome-session-bin
package in Debian) which is similar.

And there is _systemd-inhibit_ which can prevent blanking/suspending/shutdown.

Alledgedly, there is a thing called _gnome-screensaver-command_ which does the same (but I can't find it).

It turns out, preventing screen blanking is a commonly requested task.
Pick you choice, whatever floats your boat...


# For developers: Build .deb package / Releasing

This is more of a reminder to myself on how to release a new version :-)

```
  # Determine the new version:
  export coffeine_ver=0.2
  export DEBEMAIL="Sebastian Spaeth <Sebastian@SSpaeth.de>"
  
  # Set the release version in meson.build by issueing:
  meson rewrite kwargs set project / version $coffeine_ver

  # Create Changelog:
  dch -v ${coffeine_ver}-1 # new version
  dch -a                   # append new changelog entry
  dch -r -v                # release it!
  # Now edit your changelog
 
  # commit & create release git tag
  git add meson.build debian/changelog;
  git commit -m"Release v${coffeine_ver}"
  git tag -s v${coffeine_ver} -m"Release v${coffeine_ver}"
  git push origin --tags

  # Clean up local files that are not part of the branch, e.g. _build
  # or other directories.
  # Attention: THIS DELETES ALL FILES NOT IN GIT
  git clean -dfx

  # Create coffeine-X.X-Y.orig.tar.bz2 and place it in parent dir:
  tar cz --exclude=_build --exclude=.git -f ../coffeine_${coffeine_ver}.orig.tar.gz .

  # Build it: (use -uc -us to prevent it from trying to gpg sign the deb)
  debuild -sa

  # To build the arm64 .deb upload the orig.tar.gz and the .dsc file to Suses
  # OpenBuildService
```
